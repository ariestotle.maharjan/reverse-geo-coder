# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import pyautogui
import time
import pandas as pd
import pyperclip
import xlwt
from xlwt import Workbook
wb = Workbook()
sheet1 = wb.add_sheet('Sheet 1')
df = pd.read_excel(r'C:\Users\aries\Downloads\test1.xlsx') #path of excel file with coordinates
arr = df.to_numpy()
for i in range(1,20):
    row = arr[i]
    string = ','.join(str(x) for x in row)
    pyperclip.copy(string)
    y = pyperclip.paste()
    sheet1.write(i, 0, y)
    pyautogui.keyDown("alt")
    pyautogui.press("tab")
    pyautogui.keyUp("alt")
    pyautogui.moveTo(299, 170, duration=0.1)
    pyautogui.click(299, 170)
    pyautogui.hotkey("ctrlleft", "a")
    pyautogui.hotkey("ctrlleft", "v")
    pyautogui.press("enter")
    time.sleep(1.5)
    pyautogui.click(466, 707)
    x = pyperclip.paste()
    sheet1.write(i, 1, x)
    wb.save('fourth.xls') #file with addresses






